static inline
int irqEnabled()
{
    int f;
    asm volatile ( "pushf\n\t"
                   "popl %0"
                   : "=g"(f) );
    return f & ( 1 << 9 );
}
