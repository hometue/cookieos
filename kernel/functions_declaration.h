#ifndef FUN_DECLARATIONS_H
#define FUN_DECLARATIONS_H

inline void outb(unsigned short port, unsigned char val) {
    asm volatile( "outb %0, %1"
                  : : "a"(val), "Nd"(port) );
}
inline unsigned int inb(unsigned short port) {
    unsigned char ret;
    asm volatile( "inb %1, %0"
                  : "=a"(ret) : "Nd"(port) );
    return ret;
}

extern int strlen(char* str);
extern char* bool2chars(bool input);
extern bool specialbytetest(unsigned char byte);
extern char* byte2chars(unsigned char byte);
extern char* itoa(int value, char * str, int base);

#endif
