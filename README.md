This shall be an epic place to where adventure leads you...

How to get around
-----------------------------------
* `/` - contains the readme and misc
* `/build/` (contents ignored by git) - contains built files and final .iso image of operating system, the makefile is here
* `/kernel/` - contains kernel source code
* `isodir` - contains final .iso image

How to run
-----------------------------------
1. make sure you have a cross compiler placed in `~/opt/cross/` directory and grub programs (with their dependencies) installed
2. enter project`s root
3. `make` the source code in `build` directory
4. run `mk_iso.sh` in the root directory
5. attach the resulting `/build/cookieos.iso` and run your virtual machine

Things to do
-----------------------------------
1. Give the user a cookie(any 3D printer driver I can use?)
2. Code a better OS worth people actually seeing(hey, don't complain, its not easy)
3. Waste people's time by making a totally random point(check)
4. Currently, it isn't possible to have two source files with the same 
name anywhere in the kernel's code structure, because of our amazing `makefile`. 
That makefile needs to be rebuilt.

Note
-----------------------------------
If you noticed, there may be two makefiles (depends). Well...if I have left it so, please
rename makefile_std to makefile (and get rid of the other somehow) and continue using it, I
use a modified script for myself but it may cause problems (because of some additional flags for
debugging purposes, but I don't see why -O2 can cause problems) If my modified script isn't
working for you (You can have a look and modify it to your needs) use the standard one (if there
is two)
